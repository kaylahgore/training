#!/bin/bash

DATE=$(date +%H-%M-%S)
BACKUP=db-$DATE.sql
DB_HOST=$1
DB_PASSWD=$2
DB_NAME=$3
AWS_SECRET=$4
BUCKET_NAME=$5

mysqldump -u root -h $DB_HOST -p$DB_PASSWD $DB_NAME > /tmp/$BACKUP && \
export AWS_ACCCESS_KEY_ID="" && \
export AWS_SECREET_ACCESS_KEY=$AWS_SECRET && \
aws s3 cp /tmp/$BACKUP s3://$BUCKET_NAME/$BACKUP && \
echo "Uploading your Database at" date
cat /tmp/db-* | grep -i Completed


#Running the Script
#./tmp/S3.sh (Pass appro parameters)

